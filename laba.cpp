// parallel_laba_1.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//
#include <omp.h>
#include <iostream>

using namespace std;
void main()
{
	setlocale(LC_ALL, "rus");

	int n, a, b, threads_num, chunk, i = 0;
	double step, s = 0, x, y;

	cout << "Введите n - разбиение промежутка интегрирования: ";
	cin >> n;

	cout << endl << "Введите начало промежутка интегрирования: ";
	cin >> a;
	cout << endl << "Введите конец промежутка интегрирования: ";
	cin >> b;
	step = (b - a) / (double)n;
	int simple_time_start = omp_get_wtime();
	for (i = 0; i < n; i++)
	{
		x = a + i * step;
		y = x * x;
		s = s + y * step;
	}

	cout << "Время выполнения параллельной программы: " << omp_get_wtime() - simple_time_start << endl;
	cout << "Значение интеграла: " << s;
//параллельный вариант
	int parallel_time_start = omp_get_wtime();
	threads_num = omp_get_max_threads();
	chunk = n / threads_num;
	omp_set_num_threads(threads_num);
#pragma omp parallel reduction(+:s) 
	{
#pragma omp for schedule (dynamic, chunk)
		for (i = 0; i < n; i++)
		{
			x = a + i * step;
			y = x * x;
			s = s + y * step;
		}

	}
	cout << "Время выполнения параллельной программы: " << omp_get_wtime() - parallel_time_start << endl; 
	cout << "Значение интеграла: " << s;
}
